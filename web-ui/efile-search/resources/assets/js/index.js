/**
 * Created by Rajesh on 11/26/18.
 */

import React from 'react';
import {render} from 'react-dom';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import deepPurple from '@material-ui/core/colors/deepPurple';
import blue from '@material-ui/core/colors/blue';
import '../sass/app.scss';
import store from './store';
import Landing from './pages/landing';
import About from './pages/about';
import NotFound from './pages/notfound';

const theme = createMuiTheme({
    palette: {
        primary: deepPurple,
        secondary: blue,
    }
});

let Root = ({store}) => (
    <Provider store={store}>
        <MuiThemeProvider theme={theme}>
            <Router>
                <Switch>
                    <Route exact path="/" component={Landing} />
                    <Route path="/search" component={Landing} />
                    <Route path="/about" component={About} />
                    <Route component={NotFound} />
                </Switch>
            </Router>
        </MuiThemeProvider>
    </Provider>
);

Root.propTypes = {
    store: PropTypes.object.isRequired
};

render(
    <Root store={store}/>,
    document.getElementById('app')
);
