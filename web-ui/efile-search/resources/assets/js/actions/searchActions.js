/**
 * Created by Rajesh on 11/27/18.
 */

import axios from "axios";


export function getSearchResult(query, page) {
    return getResponseFromAPI(encodeURI(`/api/search?q=${query}&p=${page}`));
}


function getResponseFromAPI(url) {
    return axios.get(url, {
            headers: {'api-key': 'W2AFAE3H'},
        })
        .then((response) => {
            return response.data;
        })
        .catch((err) => {
            return err;
        });
}