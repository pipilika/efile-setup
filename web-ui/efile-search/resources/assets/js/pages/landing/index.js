/**
 * Created by Rajesh on 11/26/18.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom'
import queryString from 'query-string';

import {getSearchResult} from '../../actions/searchActions';
import InputForm from './inputForm';
import {getPaginationArray, convertNumberToBengali} from '../../common';

import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import Divider from '@material-ui/core/Divider';

const styles = theme => ({
    grid: {
        padding: theme.spacing.unit
    },
    content: {
        paddingTop: theme.spacing.unit * 3,
        paddingBottom: theme.spacing.unit * 4,
        paddingLeft: '16px',
        paddingRight: '16px',
        [theme.breakpoints.up('sm')]: {
            paddingLeft: '24px',
            paddingRight: '24px',
        },
    },
    button: {
        margin: theme.spacing.unit,
        minWidth: '30px'
    }
});

class Landing extends Component {

    constructor(props) {
        super(props);
        this.state = {
            query: '',
            page : 1,
            results: [],
            total: 0,
            pagination: []
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.renderPagination = this.renderPagination.bind(this);
        this.handlePaginationClick = this.handlePaginationClick.bind(this);
        this.fetchData = this.fetchData.bind(this);
    }

    componentDidMount(){
        let params = queryString.parse(this.props.location.search);
        if(params.q){
            this.fetchData(encodeURI(params.q), params.p);
        }
    }

    componentWillReceiveProps(nextProps){
        let params = queryString.parse(nextProps.location.search);
        if(params.q){
            this.fetchData(encodeURI(params.q), params.p);
        }
    }

    handlePaginationClick(currentPage){
        this.props.history.push(`/search?q=${this.state.query}&p=${currentPage}`);
    }

    handleSubmit(query){
        this.props.history.push(`/search?q=${query}`);
    }

    fetchData(newQuery, newPage = 1){
        getSearchResult(newQuery, newPage).then((response) => {
            if(response.status && response.data){
                this.setState({
                    query: newQuery,
                    page : newPage,
                    results: response.data,
                    total: response.total_result,
                    pagination: getPaginationArray(response.total_result, 20, newPage, 5)
                });
            }
        });
    }

    renderPagination(pages){
        let {classes} = this.props;

        return pages.map((page, index) => {
            if(index == 0){
                return(
                    <Button key={index}
                            size="small" className={classes.button}
                            disabled={this.state.page == 1}
                            onClick={() => this.handlePaginationClick(page)}
                    >
                        <Icon>keyboard_arrow_left</Icon>
                    </Button>
                );
            }else if(index == pages.length - 1){
                return(
                    <Button key={index}
                            size="small" className={classes.button}
                            disabled={this.state.page == pages[pages.length - 1]}
                            onClick={() => this.handlePaginationClick(page)}
                    >
                        <Icon>keyboard_arrow_right</Icon>
                    </Button>
                );
            }else if(page == this.state.page){
                return(
                    <Button key={index} variant="contained" color="primary" size="small" className={classes.button} onClick={() => this.handlePaginationClick(page)}>{page}</Button>
                );
            }
            return(
                <Button key={index} size="small" className={classes.button} onClick={() => this.handlePaginationClick(page)}>{page}</Button>
            );
        });
    }

    render() {
        let {classes, width} = this.props;

        let results = () => {
            if(this.state.results.length > 0){
                return this.state.results.map((item, index) => {
                    return (
                        <div key={index}>
                            <ListItem>
                                <Avatar>
                                    <Icon>drafts</Icon>
                                </Avatar>
                                <ListItemText primary={item.title} secondary={item.content}/>
                            </ListItem>
                            {index < this.state.results.length - 1 && <Divider/>}
                        </div>
                    );
                });
            }else if(this.state.query.length > 0){
                return(
                    <Typography variant="caption" style={{margin : '10px'}}>
                         দুঃখিত, আপনার অনুসন্ধানের কোন ফলাফল পাওয়া যায়নি।
                    </Typography>
                );
            }else{
                return(
                    <Typography variant="subtitle1" style={{margin : '10px'}}>
                        অনুসন্ধানের জন্য উপরের সার্চবক্সে টাইপ করুন।
                    </Typography>
                );
            }
        };

        return(
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <Grid container spacing={16} className={classes.grid}>
                            <Grid item sm={12} md={2} container justify={isWidthUp('md', width) ? 'flex-end' : 'center'}>
                                <Typography className={classes.title} variant="h6" color="inherit" noWrap>
                                    E-file Search
                                </Typography>
                            </Grid>
                            <Grid item sm={12} md={6}>
                                <InputForm onSubmit={this.handleSubmit}/>
                            </Grid>
                        </Grid>
                    </Toolbar>
                </AppBar>
                <div className={classes.content}>
                    <Grid container spacing={16} className={classes.grid}>
                        <Grid item sm={12} md={2}/>
                        <Grid item sm={12} md={6}>
                            {
                                this.state.total > 0 &&
                                <Typography variant="caption" style={{marginBottom : '10px'}}>
                                     সর্বমোট  {convertNumberToBengali(this.state.total)} টি ফলাফল পাওয়া গিয়েছে
                                </Typography>
                            }

                            <Paper>
                                <List>
                                    {results()}
                                </List>
                            </Paper>

                        </Grid>
                        <Grid item sm={12} md={4}/>
                        <Grid item sm={12} md={2}/>
                        <Grid item sm={12} md={6} container justify="flex-end">
                            {this.renderPagination(this.state.pagination)}
                        </Grid>
                    </Grid>
                </div>
            </div>
        )
    }

}

Landing.propTypes = {
    classes : PropTypes.object.isRequired
};

Landing = withWidth()(Landing);
let LandingRouter = withRouter(Landing);

export default withStyles(styles)(LandingRouter);
