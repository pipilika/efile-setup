/**
 * Created by Rajesh on 11/27/18.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import InputBase from '@material-ui/core/InputBase';
import Icon from '@material-ui/core/Icon';

const styles = theme => ({
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        transition: theme.transitions.create('width'),
        maxWidth: '100%',
        width: 350,
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
            width: 800
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
    }
});

class InputForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            query : ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit(event){
        event.preventDefault();
        if(this.props.onSubmit){
            this.props.onSubmit(encodeURI(this.state.query));
        }
    }

    handleChange(event){
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render(){
        let {classes} = this.props;

        return(
            <div className={classes.search}>
                <form onSubmit={this.handleSubmit}>
                    <div className={classes.searchIcon}>
                        <Icon>search</Icon>
                    </div>
                    <InputBase
                        placeholder="Search…"
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                        id="query" name="query" value={this.state.query} onChange={this.handleChange}
                    />
                </form>
            </div>
        );
    }
}

InputForm.propTypes = {
    classes : PropTypes.object.isRequired,
    onSubmit: PropTypes.func
};


export default withStyles(styles)(InputForm);
