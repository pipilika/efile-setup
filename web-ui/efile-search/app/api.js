/**
 * Created by Rajesh on 11/27/18.
 */

require('dotenv').config();
var url = require('url');
var express = require('express');
var request = require('request');

var router = express.Router();

var api_base = process.env.API_BASE_URL ? process.env.API_BASE_URL : 'http://10.100.222.162:1042/api/';

router.get('/', function(req, res, next) {
    res.json({
        success: true,
        status: 200,
        message: "Api is Working. Please Try correct URL"
    });
});

router.get('/search', function (req, res, next) {
    var url_parts = url.parse(req.url, true);
    var params = url_parts.query;

    var request_url = `${api_base}search?query=${params.q}&page_no=${params.p ? params.p : 1}`;

    request.get(request_url, function(error, response, body) {
        if (!error && response.statusCode === 200) {
            try {
                res.json(JSON.parse(body));
            }catch (err){
                next(err);
            }
        } else {
            next(error);
        }
    });
});

router.get('/*', function(req, res){
    res.json({
        success: true,
        status: 200,
        message: "Api is Working. Please Try correct URL"
    });
});

module.exports = router;

