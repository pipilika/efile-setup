# Pre requisite:
* Linux System
* Docker
* Docker compose 
* **8000, 8001** ports need to be opened in the Host machine.

# Setup Docker
### OS requirements
To install Docker CE, you need the 64-bit version of one of these Ubuntu versions:
* Bionic 18.04 (LTS)
* Xenial 16.04 (LTS)
* Trusty 14.04 (LTS)
>Docker CE is supported on Ubuntu on x86_64, armhf, s390x (IBM Z), and ppc64le (IBM Power) architectures.

### Uninstall old versions
Older versions of Docker were called `docker` or `docker-engine`. If these are installed, uninstall them:

<pre>sudo apt-get remove docker docker-engine docker.io</pre>

It's OK if `apt-get` reports that none of these packages are installed.

The contents of `/var/lib/docker/`, including images, containers, volumes, and networks, are preserved. The Docker CE package is now called `docker-ce`.

### Install Docker CE
Before you install Docker CE for the first time on a new host machine, you need to set up the Docker repository. Afterward, you can install and update Docker from the repository.

_SET UP THE REPOSITORY_
1. Update the apt package index:
<pre>sudo apt-get update</pre>
2. Install packages to allow apt to use a repository over HTTPS:
<pre>sudo apt-get install apt-transport-https ca-certificates curl software-properties-common</pre>


3. Add Docker's official GPG key:
<pre>curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -</pre>
>>>
(This is optional. You can move on to step 4) Verify that you now have the key with the fingerprint 9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88, by searching for the last 8 characters of the fingerprint.

$ sudo apt-key fingerprint 0EBFCD88

pub   4096R/0EBFCD88 2017-02-22\
      Key fingerprint = 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88\
uid                  Docker Release (CE deb) <docker@docker.com>\
sub   4096R/F273FCD8 2017-02-22
>>>
4. Use the following command to set up the stable repository. You always need the stable repository, even if you want to install builds from the edge or test repositories as well. To add the edge or test repository, add the word edge or test (or both) after the word stable in the commands below.

_Note:_ The `lsb_release -cs` sub-command below returns the name of your Ubuntu distribution, such as `xenial`. Sometimes, in a distribution like Linux Mint, you might need to change `$(lsb_release -cs)` to your parent Ubuntu distribution. For example, if you are using `Linux Mint Rafaela`, you could use `trusty`.

_For `x86_64 / amd64`_
<pre>sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"</pre>
>Note: Starting with Docker 17.06, stable releases are also pushed to the edge and test repositories.

_INSTALL DOCKER CE_
1. Update the apt package index.
<pre>sudo apt-get update</pre>
2. Install the latest version of Docker CE, or go to the next step to install a specific version:
<pre>sudo apt-get install docker-ce</pre>
3. Verify that Docker CE is installed correctly by running the hello-world image.
<pre>sudo docker run hello-world</pre>
>This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.

Docker CE is installed and running. The docker group is created but no users are added to it. You need to use `sudo` to run Docker commands.

# Setup Docker-Compose
_INSTALL USING PIP_

Compose can be installed from pypi using pip. If you install using pip:
<pre>sudo pip install docker-compose</pre>
>pip version 6.0 or greater is required.

# Setup Efile System
1. Keep open **8000, 8001** ports on your system.
2. Clone the repo using git from https://gitlab.com/pipilika/efile-setup.git link
3. Open terminal into the cloned git repo folder
4. Then you must have to run the following command
<pre>sudo chmod -R 777 .</pre>
5. Now run the following command to start the service:
>For the first time use the command below:
<pre>sudo docker-compose up -d</pre>
>And after then, use the command below everytime to start the service:
<pre>sudo docker-compose start</pre>
6. To stop the service run the following command:
<pre>sudo docker-compose stop</pre>


⚡ **It will take some time to start the service for first-time use. Please give the service ~5 minutes to boot up when you are starting the service for the first time.**\
⚡ **After the first time, it will take no time to boot up.**


Now to use `Efile API`, hit the following URL and follow the API documentation:
<pre>http://HOST-IP:8000/PipilikaEfileAPI-1.0/</pre>
And, to use `Efile Dashboard`, hit:
<pre>http://HOST-IP:8001</pre>
